package com.wwmxd.generator.mapper;

import com.wwmxd.common.mapper.SuperMapper;
import com.wwmxd.generator.entity.Element;

/**
 *
 * 
 *
 * @author WWMXD
 * @email 309980030@qq.com
 * @date 2018-01-02 16:21:36
 */
public interface ElementDao extends SuperMapper<Element> {
    public int deleteAll();

}